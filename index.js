#!/usr/bin/env node
const { execSync } = require('child_process')
const { red } = require('chalk')
const commander = require('commander')
const inquirer = require('inquirer')

const packageJson = require('./package.json')
const cancel = '--> Exit <--'

function execCommands (cmds = []) {
  for (const cmd of cmds) {
    try {
      execSync(cmd, { stdio: 'inherit' })
    } catch (err) {
      // Silent non-zero exit code from the proccess.
    }
  }
}

const program = new commander.Command(packageJson.name)
  .version(packageJson.version)
  .option('--spawn', 'Run command on a gnome-terminal session (Gnome Desktop Only)')
  .parse(process.argv)

let gnomeTerminal = false
if (process.platform === 'linux' && program.spawn) {
  try {
    const buffer = execSync('which gnome-terminal')
    gnomeTerminal = buffer.toString()
  } catch (err) {
    console.log()
    console.log(red('Can not find gnome-terminal... `--spawn` option will be ignored.'))
    console.log()
  }
}

console.log()
let list
try {
  list = execSync('docker ps --format "{{.Names}}" --no-trunc')
} catch (err) {
  console.log('Exiting due Docker error!')
  console.log()
  process.exit(1)
}

if (list.toString() === '') {
  console.log('There is no running containers!')
  console.log()
  process.exit(1)
}

let containerList = list.toString().split('\n')

/* remove the last (empty string) element */
containerList.pop()

containerList.push(cancel)
containerList = containerList.map((itm) => {
  return itm.split(',').find((itm) => (itm.indexOf('/') === -1))
})

inquirer
  .prompt([
    {
      type: 'list',
      name: 'container',
      message: 'Select a container:',
      choices: containerList
    }
  ])
  .then(({ container }) => {
    if (container !== cancel) {
      inquirer
        .prompt([
          {
            type: 'input',
            name: 'cmd',
            message: 'Run a command:',
            default: () => '/bin/bash'
          }
        ])
        .then(({ cmd }) => {
          console.log()

          const command = (gnomeTerminal)
            ? `${gnomeTerminal} --geometry 115x35 -- docker exec -ti ${container} ${cmd} &`
            : `docker exec -ti ${container} ${cmd}`

          execCommands([ command, 'clear' ])
          process.exit(0)
        })
    } else {
      execCommands([ 'clear' ])
      process.exit(0)
    }
  })
