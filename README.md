# docker-term

A simple node script that list all running containers and exec a user-defined command (/bin/bash by default) by calling `docker exec -ti <container> <user-command>` docker command.

## Install

```bash
$ npm install -g docker-term
```

## Usage

`docker-term [--spawn]`

<small style="font-size: 0.8rem;">Note: The `--spawn` option is a Linux only option and assumes `gnome-terminal` is installed on your system. It runs `docker exec -ti <container> <user-command>` on a new gnome-terminal session.</small>

```bash
$ docker-term

? Select a container:
  neo4j
  rabbitmq
  redis
❯ mysql
  proxy
  --> Exit <--
(Move up and down to reveal more choices)
```
